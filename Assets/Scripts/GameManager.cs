using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	static int team01Score = 0;
	static int team02Score = 0;
	public GUISkin scoreDisplay;

	public static void Score (string WallName) {
		if (WallName.Equals("LeftWall")){
		    team02Score += 1;
		}
		else if (WallName.Equals("RightWall")){
			team01Score += 1;
		}
	}

	void OnGUI () {
		GUI.skin = scoreDisplay;
		GUI.Label (new Rect (Screen.width / 2 - 150, Screen.height - 55, 100, 100), "" + team01Score);
		GUI.Label (new Rect (Screen.width / 2 + 150, Screen.height - 55, 100, 100), "" + team02Score);
						
		if (SettingsVariables.fourPlayers) {
						double timer = SwitchPlanes.timeTillSwitch;
						int intermediate = (int)(timer * 100);
						timer = (double)intermediate / 100.0;
						if (timer <= 3.00) {
								scoreDisplay.label.normal.textColor = Color.red;
						}
						GUI.Label (new Rect (Screen.width / 2 - 35, 25, 100, 100), "" + timer);
						scoreDisplay.label.normal.textColor = Color.white;
				}
	}
	
}
