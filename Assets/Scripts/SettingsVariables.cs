﻿using UnityEngine;
using System.Collections;

public class SettingsVariables : MonoBehaviour {
	
	public static bool fourPlayers = false;

	public static bool gravity = true;
	public static bool reverseGravity = true;
	public static bool noWalls = true;
	public static bool elongation = true;

	public static float ballSpeed = 20;

	public static bool menuActive = false;
	
	public static float teamOnePlayerOneSpeed = 20;
	public static KeyCode teamOnePlayerOneUp = KeyCode.W;
	public static KeyCode teamOnePlayerOneDown = KeyCode.S;

	public static float teamOnePlayerTwoSpeed = 20;
	public static KeyCode teamOnePlayerTwoUp = KeyCode.W;
	public static KeyCode teamOnePlayerTwoDown = KeyCode.S;

	public static float teamTwoPlayerOneSpeed = 20;
	public static KeyCode teamTwoPlayerOneUp = KeyCode.W;
	public static KeyCode teamTwoPlayerOneDown = KeyCode.S;

	public static float teamTwoPlayerTwoSpeed = 20;
	public static KeyCode teamTwoPlayerTwoUp = KeyCode.W;
	public static KeyCode teamTwoPlayerTwoDown = KeyCode.S;

}
