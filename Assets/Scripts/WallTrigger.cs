﻿using UnityEngine;
using System.Collections;

public class WallTrigger : MonoBehaviour {

	void OnTriggerEnter2D (Collider2D hitInfo) {
		if (hitInfo.name.Equals ("Ball")) {
			string wallName = transform.name;
			GameManager.Score (wallName);
			hitInfo.gameObject.SendMessage("ResetBall");
		}
	}

}
