﻿using UnityEngine;
using System.Collections;

public class PlayerLength : MonoBehaviour {

	public static bool elongationActivatedT1P1 = false;
	public static bool elongationActivatedT1P2 = false;
	public static bool elongationActivatedT2P1 = false;
	public static bool elongationActivatedT2P2 = false;

	public Transform TeamOnePlayerOne;
	public Transform TeamOnePlayerTwo;
	public Transform TeamTwoPlayerOne;
	public Transform TeamTwoPlayerTwo;

	public static double elongationTimerT1P1 = 0;
	public static double elongationTimerT1P2 = 0;
	public static double elongationTimerT2P1 = 0;
	public static double elongationTimerT2P2 = 0;
	
	void Update () {
		if (elongationTimerT1P1 >= 0 && elongationActivatedT1P1) {
			elongationTimerT1P1 -= Time.deltaTime;
		}
		if (elongationTimerT1P1 < 0 && elongationActivatedT1P1) {
			elongationOff(TeamOnePlayerOne);
			elongationActivatedT1P1 = false;
		}

		if (elongationTimerT1P2 >= 0 && elongationActivatedT1P2) {
			elongationTimerT1P2 -= Time.deltaTime;
		}
		if (elongationTimerT1P2 < 0 && elongationActivatedT1P2) {
			elongationOff(TeamOnePlayerTwo);
			elongationActivatedT1P2 = false;
		}

		if (elongationTimerT2P1 >= 0 && elongationActivatedT2P1) {
			elongationTimerT2P1 -= Time.deltaTime;;
		}
		if (elongationTimerT2P1 < 0 && elongationActivatedT2P1) {
			elongationOff(TeamTwoPlayerOne);
			elongationActivatedT2P1 = false;
		}

		if (elongationTimerT2P2 >= 0 && elongationActivatedT2P2) {
			elongationTimerT2P2 -= Time.deltaTime;
		}
		if (elongationTimerT2P2 < 0 && elongationActivatedT2P2) {
			elongationOff(TeamTwoPlayerTwo);
			elongationActivatedT2P2 = false;
		}
	}

	public static void elongationOn(Collision2D colInfo){
		if(colInfo.transform.name.Contains("Team 1 - Player1")){
			elongationTimerT1P1 = 20;
			elongationActivatedT1P1 = true;
			colInfo.transform.localScale = new Vector3(colInfo.transform.localScale.x, (2 * colInfo.transform.localScale.y), colInfo.transform.localScale.z);
		}
		if(colInfo.transform.name.Contains("Team 1 - Player2")){
			elongationTimerT1P2 = 20;
			elongationActivatedT1P2 = true;
			colInfo.transform.localScale = new Vector3(colInfo.transform.localScale.x, (2 * colInfo.transform.localScale.y), colInfo.transform.localScale.z);
		}
		if(colInfo.transform.name.Contains("Team 2 - Player1")){
			elongationTimerT2P1 = 20;
			elongationActivatedT2P1 = true;
			colInfo.transform.localScale = new Vector3(colInfo.transform.localScale.x, (2 * colInfo.transform.localScale.y), colInfo.transform.localScale.z);
		}
		if(colInfo.transform.name.Contains("Team 2 - Player2")){
			elongationTimerT2P2 = 20;
			elongationActivatedT2P2 = true;
			colInfo.transform.localScale = new Vector3(colInfo.transform.localScale.x, (2 * colInfo.transform.localScale.y), colInfo.transform.localScale.z);
		}
	}
	
	public void elongationOff(Transform colInfo){
		colInfo.localScale = new Vector3(colInfo.localScale.x, 1.2f, colInfo.localScale.z);
	}


}
