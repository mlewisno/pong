﻿using UnityEngine;
using System.Collections;

public class MenuScript : MonoBehaviour
{

	public Font ourFont;
	
	void OnGUI()
	{
		const int buttonWidth = 300;
		const int buttonHeight = 40;
		
		GUI.backgroundColor = Color.black;
		GUI.skin.font = ourFont;
		
		if (
			GUI.Button(
				new Rect(
					Screen.width / 2 - (buttonWidth / 2),
					(2 * Screen.height / 4) - (buttonHeight / 2),
					buttonWidth,
					buttonHeight
					),
				"Play Pong!"
				)
			)
		{
			Application.LoadLevel("Stage1");
		}
		
		if (
			GUI.Button(
				new Rect(
					Screen.width / 2 - (buttonWidth / 2),
					(2 * Screen.height / 4) + (buttonHeight * 1.15f) - (buttonHeight / 2),
					buttonWidth,
					buttonHeight
					),
				"Options"
				)
			)
		{
			Application.LoadLevel("ControlsMenu");
		}
		
		if (
			GUI.Button(
				new Rect(
					Screen.width / 2 - (buttonWidth / 2),
					(2 * Screen.height / 4) + (buttonHeight * 1.15f) + (buttonHeight / 2 + buttonHeight*0.15f),
					buttonWidth,
					buttonHeight
					),
				"Credits"
				)
			)
		{
			Application.LoadLevel("Credits");
		}
		
		if (
			GUI.Button(
				new Rect(
					Screen.width / 2 - (buttonWidth / 2),
					(2 * Screen.height / 4) + (buttonHeight * 1.15f) + (buttonHeight * 1.65f),
					buttonWidth,
					buttonHeight
					),
				"Exit Game"
				)
			)
		{
			Application.Quit ();
		}
	}
	
}
