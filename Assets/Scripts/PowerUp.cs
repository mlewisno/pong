﻿using UnityEngine;
using System.Collections;

public class PowerUp : MonoBehaviour {

	public Transform ball;

	void Start()
	{
		Destroy(gameObject, 20);
	}

	void OnTriggerEnter2D (Collider2D hitInfo) {
		if (hitInfo.name.Equals ("Ball")) {
			hitInfo.gameObject.GetComponent<BallControl>().ActivatePowerup(this.collider2D);
			Destroy (gameObject);
		}
	}

}
