using UnityEngine;
using System.Collections;

public class BallControl : MonoBehaviour {

	public Vector3 velocity;

	public BoxCollider2D topWall;
	public BoxCollider2D bottomWall;
	public Camera mainCamera;

	public float duration = 20;
	float gravityTimer = 0;
	bool gravityActivated = false;
	float reverseGravityTimer = 0;
	bool reverseGravityActivated = false;

	Collision2D lastPlayer;
	bool lastPlayerValueSet = false;

	public void PauseBall(){
		velocity = rigidbody2D.velocity;
		rigidbody2D.velocity = new Vector3(0, 0, 0);
	}

	public void UnPause(){
		rigidbody2D.velocity = velocity;
	}

	IEnumerator Start () {
		yield return new WaitForSeconds(2);
		BallStart ();
	}

	void OnCollisionEnter2D (Collision2D colInfo) {
		 if (colInfo.collider.tag == "Player") {
			lastPlayer = colInfo;
			lastPlayerValueSet = true;
			float velY = rigidbody2D.velocity.y;
			rigidbody2D.velocity = rigidbody2D.velocity - new Vector2 (0, velY/2);
			rigidbody2D.velocity = rigidbody2D.velocity + new Vector2 (0, colInfo.collider.rigidbody2D.velocity.y/3);
		}
	}
	
	public void ActivatePowerup (Collider2D hitInfo) {
		if(hitInfo.name.Contains("PowerUpGravity")){
			gravityActivated = true;
			gravityTimer = 6;
			gravityOn ();
		}
		else if(hitInfo.name.Contains("PowerUpReverseGravity")){
			reverseGravityActivated = true;
			reverseGravityTimer = 6;
			reverse_gravityOn ();
		}
		else if(hitInfo.name.Contains("PowerUpElongation")){
			if(lastPlayerValueSet){
				PlayerLength.elongationOn(lastPlayer);
			}
		}
		else if(hitInfo.name.Contains("PowerUpNoWalls")){
			WallTransform.topWall = this.topWall;
			WallTransform.bottomWall = this.bottomWall;
			PlayerControls.topWall = this.topWall;
			PlayerControls.bottomWall = this.bottomWall;
			WallTransform.transformOn();
		}
	}

	void Update () {
		if (gravityTimer >= 0 && gravityActivated) {
			gravityTimer -= Time.deltaTime;
		}
		else if (gravityTimer < 0 && gravityActivated) {
			gravityOff();
		}

		if (reverseGravityTimer >= 0 && reverseGravityActivated) {
			reverseGravityTimer -= Time.deltaTime;
		}
		else if (reverseGravityTimer < 0 && reverseGravityActivated) {
			reverse_gravityOff();
		}

	}
	
	void gravityOn(){
		this.rigidbody2D.gravityScale = 0.6f;
	}

	void gravityOff(){
		this.rigidbody2D.gravityScale = 0;
	}

	void reverse_gravityOn(){
		this.rigidbody2D.gravityScale = -0.6f;
	}
	
	void reverse_gravityOff(){
		this.rigidbody2D.gravityScale = 0;
	}

	IEnumerator ResetBall(){
		rigidbody2D.velocity = new Vector2 (0, 0);
		float zpos = transform.position.z;
		transform.position = new Vector3 (0, 0, zpos);
		yield return new WaitForSeconds(0.5f);
		lastPlayerValueSet = false;
		if(!SettingsVariables.menuActive){
			BallStart ();
		}

	}

	void BallStart (){
		int randomNumber = Random.Range (0, 4);

		switch (randomNumber){
		case 0:
			rigidbody2D.AddForce (new Vector2 (-80, -10));
			break;
		case 1:
			rigidbody2D.AddForce (new Vector2 (-80, 10));
			break;
		case 2:
			rigidbody2D.AddForce (new Vector2 (80, 10));
			break;
		default:
			rigidbody2D.AddForce (new Vector2 (80, -10));
			break;
		}
	}

	void TeleportBall(){
		if (this.transform.position.y > 0) {
			this.transform.position = new Vector3(this.transform.position.x, bottomWall.center.y + 1f, this.transform.position.z);
		}
		else{
			this.transform.position = new Vector3(this.transform.position.x, topWall.center.y - 1f, this.transform.position.z);
		}
	}
}
