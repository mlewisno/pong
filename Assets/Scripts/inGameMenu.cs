﻿using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour {

	const int buttonWidth = 500;
	const int buttonHeight = 100;
	public Font ourFont;
	public Font headerFont;
	public Texture2D whiteColor;
	public GUISkin mySkin;

	public Transform ball;
	public Vector3 storedVelocity;

	public bool topPlayers = false;
	public bool bottomPlayers = false;
	public Transform T1p1;
	public Transform T1p2;
	public Transform T2p1;
	public Transform T2p2;

	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape) && SettingsVariables.menuActive){
			SettingsVariables.menuActive = false;
			Time.timeScale = 1;
		}
		else if(Input.GetKeyDown(KeyCode.Escape) && !SettingsVariables.menuActive){
			SettingsVariables.menuActive = true;
			Time.timeScale = 0;
		}
	}

	void OnGUI()
	{
		if(SettingsVariables.menuActive){
			const int buttonWidth = 300;
			const int buttonHeight = 40;
			
			GUI.backgroundColor = Color.black;
			GUI.skin.font = ourFont;
			
			// Draw a button to go back to the main menu
			if (
				GUI.Button(
				// Center in X, 2/3 of the height in Y
				new Rect(
				Screen.width / 2 - (buttonWidth / 2),
				(2 * Screen.height / 4) - (buttonHeight * 0.65f),
				buttonWidth,
				buttonHeight
				),
				"Main Menu"
				)
				)
			{
				SettingsVariables.menuActive = false;
				Time.timeScale = 1;
				Application.LoadLevel ("Menu");
			}
		}
	}
}
