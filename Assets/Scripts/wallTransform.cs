﻿using UnityEngine;
using System.Collections;

public class WallTransform : MonoBehaviour {

	public static BoxCollider2D topWall;
	public static BoxCollider2D bottomWall;
	public static double wallTimer = 0;
	public static bool wallActivated = false;

	void Update () {
		if (wallTimer >= 0 && wallActivated) {
			wallTimer -= Time.deltaTime;
		}
		if (wallTimer < 0 && wallActivated) {
			transformOff();
			wallActivated = false;
		}
	}

	public static void transformOn(){
		topWall.isTrigger = true;
		bottomWall.isTrigger = true;
		wallActivated = true;
		wallTimer = 9;
	}

	void transformOff(){
		topWall.isTrigger = false;
		bottomWall.isTrigger = false;
	}

	void OnTriggerEnter2D (Collider2D hitInfo) {
		if (wallActivated){
			if (hitInfo.name.Equals ("Ball")) {
				hitInfo.gameObject.SendMessage("TeleportBall");
			}
			else if (hitInfo.name.Contains ("Player")) {
				hitInfo.gameObject.SendMessage("TeleportPlayer");
			}
		}
	}

}
