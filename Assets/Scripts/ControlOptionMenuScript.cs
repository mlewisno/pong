using UnityEngine;
using System.Collections;

public class ControlOptionMenuScript : MonoBehaviour
{
	const int buttonWidth = 500;
	const int buttonHeight = 100;
	public Font ourFont;
	public Font headerFont;
	public Texture2D whiteColor;
	public GUISkin mySkin;

	void OnGUI () {
		GUI.skin.font = ourFont;
		GUI.skin = mySkin;
		GUI.backgroundColor = Color.white;
		GUI.contentColor = Color.black;
		GUI.skin.button.hover.background = whiteColor;
		GUI.skin.button.onHover.background = whiteColor;
		GUI.skin.button.active.background = whiteColor;
		GUI.skin.button.normal.background = whiteColor;
		// Sets up the two highest level areas
		// Area One
		GUILayout.BeginArea( new Rect (Screen.width * 0.1f, Screen.height * 0.001f, Screen.width, 0.9f * Screen.height));
			GUILayout.BeginHorizontal ();
				// Sets up the left section of the menu which handles general settings
				GUILayout.BeginArea( new Rect (0, 0, Screen.width/2 - Screen.width*0.1f, 0.9f * Screen.height));
					GUILayout.BeginVertical();
						GUI.skin.font = headerFont;
						GUILayout.Label("General Settings");
						GUI.skin.font = ourFont;
							SettingsVariables.fourPlayers = GUILayout.Toggle (SettingsVariables.fourPlayers, "4 Players");
						GUI.skin.font = headerFont;
						GUILayout.Label("PowerUps");
						GUI.skin.font = ourFont;
							SettingsVariables.gravity = GUILayout.Toggle (SettingsVariables.gravity, "Gravity");
							SettingsVariables.reverseGravity = GUILayout.Toggle (SettingsVariables.reverseGravity, "Reverse Gravity");
							SettingsVariables.elongation = GUILayout.Toggle (SettingsVariables.elongation, "Paddle Elongation");
							SettingsVariables.noWalls = GUILayout.Toggle (SettingsVariables.noWalls, "Transformation");
					GUILayout.EndVertical();
				GUILayout.EndArea();
				
				
				GUILayout.BeginArea( new Rect (Screen.width/2 - Screen.width * 0.1f, 0, Screen.width/2 - Screen.width*0.1f, 0.9f * Screen.height));
					GUILayout.BeginVertical();
						// Label for the section
						GUI.skin.font = headerFont;
						GUILayout.Label("Player Settings");
						GUI.skin.font = ourFont;
						// Label and slider for the first ediutable parameter, player sensetivity
						GUILayout.Label ("Team One Player 1 Sensetivity = " + Shorten(SettingsVariables.teamOnePlayerOneSpeed));
							SettingsVariables.teamOnePlayerOneSpeed = GUILayout.HorizontalSlider (SettingsVariables.teamOnePlayerOneSpeed, 20.0f, 30.0f);
							GUILayout.Label ("Team One Player 2 Sensetivity = " + Shorten(SettingsVariables.teamOnePlayerTwoSpeed));
							SettingsVariables.teamOnePlayerTwoSpeed = GUILayout.HorizontalSlider (SettingsVariables.teamOnePlayerTwoSpeed, 20.0f, 30.0f);
							GUILayout.Label ("Team Two Player 1 Sensetivity = " + Shorten(SettingsVariables.teamTwoPlayerOneSpeed));
							SettingsVariables.teamTwoPlayerOneSpeed = GUILayout.HorizontalSlider (SettingsVariables.teamTwoPlayerOneSpeed, 20.0f, 30.0f);
							GUILayout.Label ("Team Two Player 2 Sensetivity = " + Shorten(SettingsVariables.teamTwoPlayerTwoSpeed));
							SettingsVariables.teamTwoPlayerTwoSpeed = GUILayout.HorizontalSlider (SettingsVariables.teamTwoPlayerTwoSpeed, 20.0f, 30.0f);
					GUILayout.EndVertical ();
				GUILayout.EndArea();
			GUILayout.EndHorizontal ();
		GUILayout.EndArea();

		// Area Two
		GUILayout.BeginArea (new Rect (0, Screen.height * 0.88f, Screen.width, Screen.height * 0.1f));
			GUILayout.BeginHorizontal ();
				if (GUILayout.Button("Main Menu"))
				{
					Application.LoadLevel ("Menu");
				}
			GUILayout.EndHorizontal ();
		GUILayout.EndArea();
	}

	float Shorten(float number){
			number = number * 10;
			int middle = (int) number;
			number = middle/10f;
			return number;
	}
	
}
