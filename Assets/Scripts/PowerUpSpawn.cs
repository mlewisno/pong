using UnityEngine;
using System.Collections;

public class PowerUpSpawn : MonoBehaviour {

		public static double spawnTimer;
		public Camera mainCamera;

		Transform[] Powerups;

		public Transform GravityPowerUp;
		public Transform ReverseGravityPowerUp;
		public Transform NoWallPowerUp;
		public Transform ElongationPowerUp;

		public BoxCollider2D topWall;
		public BoxCollider2D bottomWall;
		public BoxCollider2D leftWall;
		public BoxCollider2D rightWall;

		void Start ()
		{
				int size = 0;
				spawnTimer = 5;

				if (SettingsVariables.gravity) {
						size ++;
				}
				if (SettingsVariables.reverseGravity) {
						size ++;
				}
				if (SettingsVariables.noWalls) {
						size ++;
				}
				if (SettingsVariables.elongation) {
						size ++;
				}

				Powerups = new Transform[size];

				if (SettingsVariables.gravity) {
						addToArray (GravityPowerUp);
				}
				if (SettingsVariables.reverseGravity) {
						addToArray (ReverseGravityPowerUp);
				}
				if (SettingsVariables.noWalls) {
						addToArray (NoWallPowerUp);
				}
				if (SettingsVariables.elongation) {
						addToArray (ElongationPowerUp);
				}
		}

		void addToArray (Transform thingy)
		{
				for (int i = 0; i < Powerups.Length; i++) {
						if (Powerups [i] == null) {
								Powerups [i] = thingy;
								break;
						}
				}
		}

		void Update ()
		{
				if (spawnTimer <= 0) {
						PickPowerUp ();
						spawnTimer = Random.Range (2.0f, 10.5f);
				} else {
						spawnTimer -= Time.deltaTime;
				}
		}

		void PickPowerUp ()
		{
			Spawn (Powerups [Random.Range (0, Powerups.Length)]);
		}

		void Spawn (Transform thingy)
		{
				var powerUp = Instantiate (thingy) as Transform;
				float[] coords = PickSpawnCoordinates ();
				powerUp.position = new Vector2 (coords [0], coords [1]);
		}

		float[] PickSpawnCoordinates ()
		{
				float[] coords = new float[2];
				coords [0] = Random.Range (leftWall.center.x + 15.5f, rightWall.center.x - 15.5f);
				coords [1] = Random.Range (bottomWall.center.y + 10, topWall.center.y - 10);
				return coords;
		}
}
