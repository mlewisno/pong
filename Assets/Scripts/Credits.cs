﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {
	
	public Camera mainCamera;
	Vector2 screenTop;
	
	void Start () {
		screenTop = new Vector2 (0f, mainCamera.ScreenToWorldPoint (new Vector3 (0, Screen.height, 0)).y + 0.5f);
	}
	
	void Update () {
		if(transform.position.y - transform.localScale.y > screenTop.y){
			Application.LoadLevel("Menu");
		}
		transform.rigidbody2D.velocity = new Vector3(0, 1.5f, 0);
	}
}